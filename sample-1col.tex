%% The first command in your LaTeX source must be the \documentclass command.
%%
%% Options:
%% twocolumn : Two column layout.
%% hf: enable header and footer.
\documentclass[
% twocolumn,
% hf,
]{ceurart}

%%
%% One can fix some overfulls
% \sloppy

%%
%% Minted listings support 
%% Need pygment <http://pygments.org/> <http://pypi.python.org/pypi/Pygments>
\usepackage{minted}
\usepackage{listings}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{booktabs}
\usepackage{subcaption}
\usepackage{color}
\definecolor{editorGray}{rgb}{0.95, 0.95, 0.95}
\definecolor{editorOcher}{rgb}{1, 0.5, 0} % #FF7F00 -> rgb(239, 169, 0)
\definecolor{editorGreen}{rgb}{0, 0.5, 0} % #007C00 -> rgb(0, 124, 0)
\usepackage{upquote}
\usepackage{listings}
\usepackage{tabularx,ragged2e}
\usepackage{makecell}
\usepackage{xspace}
\usepackage{url}
\usepackage{hyperref}
%% auto break lines
\setminted{breaklines=true}


%%
%% end of the preamble, start of the body of the document source.
\begin{document}

%%
%% Rights management information.
%% CC-BY is default license.
\copyrightyear{2021}
\copyrightclause{Copyright for this paper by its authors.
  Use permitted under Creative Commons License Attribution 4.0
  International (CC BY 4.0).}

%%
%% This command is for the conference information
\conference{The 22nd Belgium-Netherlands Software Evolution Workshop
  Nijmegen, 27-28 November 2023}

%%
%% The "title" command
\title{Using the spiral algorithm for evolving software cities in VR.}

%%
%% The "author" command and its associated commands are used to define
%% the authors and their affiliations.
\author[1]{David Moreno-Lumbreras}[%
  orcid=0000-0002-5454-7808,
  email=david.morenolu@urjc.es,
  url=https://dlumbrer.github.io/,
]

\address[1]{EIF @ Universidad Rey Juan Carlos, Spain}

\author[1]{Gregorio Robles}[%
  orcid=0000-0002-1442-6761,
  email=gregorio.robles@urjc.es,
  url=http://gsyc.urjc.es/~grex,
]

\author[1]{Jesus M. Gonzalez-Barahona}[%
  orcid=0000-0001-9682-460X,
  email=jesus.gonzalez.barahona@urjc.es,
  url=http://gsyc.es/~jgb,
]


% Useful commands 
\newcommand{\ie}{\emph{i.e.,}\xspace}
\newcommand{\eg}{\emph{e.g.,}\xspace}
\newcommand{\etc}{\emph{etc.}\xspace}
\newcommand{\etal}{\emph{et~al.}\xspace}
\newcommand{\secref}[1]{Section~\ref{#1}\xspace}
\newcommand{\chapref}[1]{Chapter~\ref{#1}\xspace}
\newcommand{\appref}[1]{Appendix~\ref{#1}\xspace}
\newcommand{\figref}[1]{Figure~\ref{#1}\xspace}
\newcommand{\subfigref}[2]{Fig.~\ref{#1}-{#2}\xspace}
\newcommand{\listref}[1]{Listing~\ref{#1}\xspace}
\renewcommand{\tabref}[1]{Table~\ref{#1}\xspace}
\newcommand{\tool}[1]{{\textit{#1}}\xspace}
\newcommand{\onscreen}{on-screen\xspace}

\newcommand{\babia}{\tool{BabiaXR}}
\newcommand{\kibana}{\tool{Kibana}}
\newcommand{\chaoss}{\tool{CHAOSS}}
\newcommand{\openshift}{\tool{OpenShift}}
\newcommand{\babiacodecity}{\tool{\babia-\codecity}}
\newcommand{\elasticsearch}{\tool{Elasticsearch}}
\newcommand{\grimoirelab}{\tool{GrimoireLab}}
\newcommand{\babiapref}{babia}
\newcommand{\babiaurl}{\urltt{https://babiaxr.gitlab.io}}
\newcommand{\babiarepo}{\urltt{https://gitlab.com/babiaxr/aframe-babia-components}}
\newcommand{\babiareplicationpackage}{\urltt{https://doi.org/10.5281/zenodo.7828691}}
\newcommand{\babianpm}{\urltt{https://npmjs.org/package/aframe-babia-components}}
\newcommand{\babiaothersone}{\urltt{https://gitlab.com/babiaxr/aframe-babia-components/-/tree/master/tools/generate\_repository\_data}}
\newcommand{\babiaotherstwo}{\urltt{https://gitlab.com/babiaxr/aframe-babia-components/-/tree/master/tools/generate\_from\_es}}
\newcommand{\babiaothersthree}{\urltt{https://gitlab.com/babiaxr/aframe-babia-components/-/tree/master/docs/APIs}}
\newcommand{\babiadiagram}{img/citydiagram.png}
\newcommand{\webvr}{\tool{WebVR}}
\newcommand{\webxr}{\tool{WebXR}}
\newcommand{\webgl}{\tool{WebGL}}
\newcommand{\codecity}{\tool{Codecity}}
\newcommand{\aframe}{\tool{A-Frame}}
\newcommand{\graal}{\tool{Graal}}
\newcommand{\npm}{\tool{NPM}}
\newcommand{\node}{\tool{NodeJS}}
\newcommand{\jet}{\tool{JetUML}}




%%
%% The abstract is a short summary of the work to be presented in the
%% article.
\begin{abstract}
  dads
\end{abstract}

% %%
% %% Keywords. The author(s) should pick words that accurately describe
% %% the work being presented. Separate the keywords with commas.
\begin{keywords}
  code city metaphor \sep
  software evolution \sep
  software visualization \sep
  \codecity \sep
  data visualization \sep
  virtual reality \sep
  web \sep
  3D
\end{keywords}


%%
%% This command processes the author and affiliation and title
%% information and builds the first part of the formatted document.
\maketitle

\section{Abstract}

The city metaphor is probably the most successful visualization for source code metrics in a 3D environment. It was proposed for the first time by Munro \etal~\cite{sworld}, and had its greatest exponent with the approach presented by Wettel \etal, which described \codecity~\cite{codecity}, its first implementation. The original \codecity creates cities that look real, due to the combination of layouts, topologies, and metric mappings applied at an appropriate level of granularity. It shows object-oriented software systems as cities that can be intuitively explored, by mapping metrics to features (height, size, color) of the buildings, and placing those buildings in locations related to their position in the object hierarchy. The city metaphor offers a clear notion of locality, supporting orientation, and does not hide the underlying structural complexity that cannot be oversimplified, and its use in the data visualization has been widely explored. Treemaps are a common choice for arranging these elements hierarchically, aiding user navigation. However, while treemaps excel at clarifying complex software structures and identifying bottlenecks, they face challenges in representing evolving software systems. Adding or removing components requires costly recalculations due to the need to optimize component sizes within rectangles. This process can lead to significant layout changes, potentially hindering user understanding of system evolution and its structural organization. Thus, while treemaps are effective for representing software systems initially, their limitations in handling changes should be considered for software evolution visualization~\cite{treemapsurvey}. On the other hand, the spiral algorithm has been proposed as a viable alternative for representing the evolution of software systems. Unlike treemaps, spiral layouts are able to represent changes in the number of elements without requiring major changes in the visualization.  In this way, the spiral algorithm can be used as a more scalable solution for representing the evolution of software systems, particularly when dealing with dynamic changes in the number of software elements.

% \section{Our approach}

We proposed a city metaphor visualization that evolves through the use of the spiral algorithm to rearrange the buildings and quarters, as part of \babia's~\cite{moreno2022:babiaxr} component set, which includes the navigation bar for controlling the evolution. In this algorithm, the initial building is situated at the center of a spiral, and subsequent buildings are positioned in a spiraling manner around it. This layout approach is also applied to groups of buildings at the same hierarchical level. To provide a clearer illustration of this approach, \figref{fig:cities} presents an example of the layout generated by our evolving city implementation. The spiral algorithm proves to be more effective in representing the evolution of a project when new elements, such as buildings, emerge and disappear in any place in the hierarchy. In contrast, rectangle packing may cause existing buildings to shift to accommodate new ones, thereby disrupting the perceivable relationship between the buildings' prior and new locations.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.4\columnwidth]{img/jetumlnew.png}
  \caption{Evolving city of the \jet project}
  \label{fig:cities}
\end{figure}

This approach utilizes software metrics to map features of the buildings. Each building is associated with a file, and its height, base, and color are determined by software metrics. Each district corresponds to a directory that can contain files and subdirectories. These features are interactive, allowing users to explore the city using cursor keys in the on-screen mode and by walking in the physical world in Virtual Reality. Users can also view data about buildings of interest. In the on-screen mode, hovering the cursor over a building opens a tooltip displaying the file name and metric values. In VR, the same feature is facilitated by the raycaster provided by the VR controller, which serves as the controller pointing mechanism. There is a live demo for this scene that can be visited in any device with a modern browser installed, available at \url{https://thesis-dlumbrer.gitlab.io/benevol2023}.

%%
%% Define the bibliography file to be used
\bibliography{sample-ceur}

%%


\end{document}

%%
%% End of file
