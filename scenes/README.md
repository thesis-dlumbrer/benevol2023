# BabiaXR CodeCity Evolution

## Scene

[Go here to see the scene](https://thesis-dlumbrer.gitlab.io/sattose2023)

### Deploy them locally

The easiest way to do it is to go to the `scenes` folder and deploy the `scenes/index.html` file within a simple HTTP server (i.e. node http-server or python simple http server).

(Optional) Examples of http servers deploy:
```shell
# Using node [1]
$> npm install -g http-server
$> http-server

# Using python 2 [2]
$> python -m SimpleHTTPServer

# Using python 3 [3]
$> python -m http.server
```